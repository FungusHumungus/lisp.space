(in-package :cl-user)
(defpackage lispspace-test-asd
  (:use :cl :asdf))
(in-package :lispspace-test-asd)

(defsystem lispspace-test
  :author "Stephen Wakely"
  :license ""
  :depends-on (:lispspace
               :prove)
  :components ((:module "t"
                :components
                ((:file "lispspace"))))
  :perform (load-op :after (op c) (asdf:clear-system c)))
