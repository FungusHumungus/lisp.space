(in-package :cl-user)
(defpackage lispspace.web
  (:use :cl
   :caveman2
        :lispspace.config
   :lispspace.view
        :lispspace.db
   :datafly
   :cl-who
        :sxql)
  (:export :*web*))
(in-package :lispspace.web)

;;
;; Application

(defclass <web> (<app>) ())
(defvar *web* (make-instance '<web>))
(clear-routing-rules *web*)

(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

(defun domain
    (url)
  (if url
      (quri:uri-host (quri:uri url))
      ""))

(defun get-initials
    (author)
  (string-upcase (subseq author 0 1)))

(defun avatar-html (out post)
  (who:with-html-output (out)
    (who:htm (:div :class "post-avatars post-module"
                   (:a :href (mkstr "https://lisp.space/users/" (gethash 'lispspace.db::author post))
                       :class "avatar-link avatar-small author-avatar"
                       (:div :class "avatar avatar-circle"
                             (:span :class "avatar-initials"
                                    (who:fmt "~A" (get-initials (gethash 'lispspace.db::author post))))))))))

(defun post-html (out post)
  (who:with-html-output (out)
    (who:htm (:div :class "post"
                   (:div :class "post-content"
                         (:div :class "post-info"
                               (:h3 :class "post-heading"
                                    (:a :class "post-title"
                                        :href (gethash 'lispspace.db::url post)
                                        (who:fmt "~A" (gethash 'lispspace.db::title post)))
                                    "&nbsp"
                                    (:span :class "post-domain"
                                           (who:fmt "~A" (domain (gethash `url post)))))
                               (:div :class "post-meta"
                                     (:a :class "post-author"
                                         :href (mkstr "https://lisp.space/users/" (gethash 'lispspace.db::author post))
                                         (who:fmt "~A" (gethash 'lispspace.db::author post)))
                                     "&nbsp;"
                                     (:div :class "post-meta-item"
                                           (:a :class "comments-link"
                                               :href (mkstr "https://lisp.space/posts/" (gethash 'lispspace.db::id post))
                                               (:span :class "comments-count"
                                                      (who:fmt " ~A " (gethash 'lispspace.db::comment-count post)))
                                               (:span :class "comments-action"
                                                      "Comments"))))))
                   (avatar-html out post)))))


;;
;; Routing rules
(defun home-html ()
  (who:with-html-output-to-string (out nil :prologue t)
    (:html
     (:head
      (:link :rel "stylesheet"
             :type "text/css"
             :href "css/style.css"))
     (:body
      (:div :class "outer-wrapper top-nav"
            (:div :class "inner-wrapper template-posts_list"
                  (:header :class "header dark-bg"
                           (:h1 :class "logo header-module"
                                "Lisp Space"))
                  (:div :class "content-wrapper"
                        (:h3 :class "grid tagline"
                             (:span "A Space for Parantheses"))
                        (:div :class "posts-wrapper grid grid-module"
                              (:div :class "posts list posts-list"
                                    (mapcar #'(lambda (post) (post-html out post)) (lispspace.db:load-posts)))))))))))

(defroute "/" ()
  ()
  (home-html))
;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
