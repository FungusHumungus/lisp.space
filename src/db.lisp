(in-package :cl-user)
(defpackage lispspace.db
  (:use :cl)
  (:import-from :lispspace.config
   :config)
  (:import-from :datafly
   :*connection*
                :connect-cached)
  (:export :connection-settings
   :db
   :load-posts
           :with-connection))
(in-package :lispspace.db)

(defun connection-settings (&optional (db :maindb))
  (cdr (assoc db (config :databases))))

(defun db (&optional (db :maindb))
  (apply #'connect-cached (connection-settings db)))

(defmacro with-connection (conn &body body)
  `(let ((*connection* ,conn))
     ,@body))

(defun mongo->hash
    (doc fields)
  "Convert the mongo record into a hash table"
  (let ((hash (make-hash-table)))
    ;; Always use the id
    (setf (gethash 'id hash) (cl-mongo:doc-id doc))
    (dolist (field fields)
      (if (atom field)
          (setf (gethash field hash) (cl-mongo:get-element (string field) doc))
          (setf (gethash (car field) hash) (cl-mongo:get-element (cdr field) doc))))
    hash))

(defun load-posts ()
  (cl-mongo:with-mongo-connection (:host "localhost" :port 3001 :db "meteor")
    (let ((posts (second (cl-mongo:db.sort "posts" :all :asc nil :field "score" :limit 10))))
      (mapcar #'(lambda (post) (mongo->hash post '((comment-count "commentCount")
                                                   (author "author")
                                                   (url "url")
                                                   (title "title"))))
              posts))))
