(in-package :cl-user)
(defpackage lispspace-asd
  (:use :cl :asdf))
(in-package :lispspace-asd)

(defsystem lispspace
  :version "0.1"
  :author "Stephen Wakely"
  :license ""
  :depends-on (:clack
               :caveman2
               :envy
               :cl-ppcre

               ;; HTML Template
               :cl-who

               ;; for DB
               :cl-mongo
               :datafly
               :sxql)
  :components ((:module "src"
                :components
                ((:file "main" :depends-on ("config" "view" "db"))
                 (:file "web" :depends-on ("view"))
                 (:file "view" :depends-on ("config"))
                 (:file "db" :depends-on ("config"))
                 (:file "config"))))
  :description ""
  :in-order-to ((test-op (load-op lispspace-test))))
